package DETOURBE.MarcAntoine.tp2;

public class Country {
    private String region;
    private String subregion;
    private String name;
    private String flag;
    private String countryCapital;
    public Country(final String countryCode, final String countryName, final String countryFlag, final String countryCapital) {

        this.region = countryCode; this.name = countryName;
        this.flag = countryFlag;
        this.countryCapital = countryCapital;
    }
    public String getRegion()
    {
        return region;
    }
    public void setRegione(final String countryCode)
    {
        this.region = countryCode;
    }
    public String getName() {
        return name;
    }
    public void setRegion(final String countryName) {
        this.name = countryName;
    }
    public String getFlag() {
        return flag;
    }
    public void setFlag(String mCountryFlag) {
        this.flag = mCountryFlag;
    }
    public String getCountryCapital() {
        return countryCapital;
    }
    public void setCountryCapital(String mCountryCapital) {
        this.countryCapital = mCountryCapital;
    }
    @Override
    public String toString() {
        return "Country{" + "countryCode='" + region + '\'' + ", countryName='" + name + '\'' + '}';
    }
}



