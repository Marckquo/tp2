package DETOURBE.MarcAntoine.tp2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

public class CountryViewHolder extends RecyclerView.ViewHolder {

    private Country mCountry;
    private final TextView mCountryName;
    private final TextView mCountryCode;
    private final ImageView mCountryFlag;

    public CountryViewHolder(final View itemView, final OnCountryClickListener onCountryClickListener)
    {
        super(itemView);
        mCountryName = itemView.findViewById(R.id.country_name);
        mCountryCode = itemView.findViewById(R.id.country_code);
        mCountryFlag = itemView.findViewById(R.id.country_flag);


        mCountryFlag.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                onCountryClickListener.onFlagClicked(mCountry);
            }
        });
        mCountryName.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                onCountryClickListener.onNameClicked(mCountry);
            }
        });

    }

    public void bind(Country country)
    {
        mCountry = country;
        mCountryName.setText(country.getName());
        mCountryCode.setText(country.getRegion());
      //  mCountryFlag.setImageResource(mCountry.getFlag());
    }
    public static CountryViewHolder newInstance(ViewGroup parent, final OnCountryClickListener onCountryClickListener)
    {
        return new CountryViewHolder( LayoutInflater.from(parent.getContext()).inflate( R.layout.lcountry, parent, false), onCountryClickListener);
    }
}