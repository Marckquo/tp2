package DETOURBE.MarcAntoine.tp2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private String API_BASE_URL = "https://restcountries.eu/";
    private ArrayList<Country> country = new ArrayList<>();


    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState); setContentView(R.layout.activity_main);

        callWithRetrofit();
// Get a reference to the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.list);


// Create and set the layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
// Set the adaptor
        CountryAdapter adapter = new CountryAdapter();
        recyclerView.setAdapter(adapter);
// Load data into the adapter.

        adapter.setCountryList(country);
        adapter.setOnCountryClickListener(new OnCountryClickListener() {

            @Override
        public void onNameClicked(final Country country) {
            Toast.makeText(MainActivity.this, "La capitale de " + country.getName() + " est " + country.getCountryCapital(), Toast.LENGTH_LONG).show();
        }
            @Override
            public void onFlagClicked(final Country country)
            {
                Toast.makeText(MainActivity.this, "C'est le drapeau de " + country.getName(), Toast.LENGTH_LONG).show();
            }
        });


    }

    private void callWithRetrofit()
    {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        MyService service = retrofit.create(MyService.class);
        Call<List<Country>> listCountry = service.getCountry();
        listCountry.enqueue(new Callback<List<Country>>()
        {
            @Override
            public void onResponse(Call<List<Country>> call, Response<List<Country>> response)
            {
                country.addAll(response.body());



                Log.d("joyPAD","on a retrouvé " + country.size() + " pays");
                Toast.makeText(MainActivity.this, country.get(0).getName(), Toast.LENGTH_LONG).show();
                RecyclerView recyclerView = findViewById(R.id.list);


// Create and set the layout manager
                LinearLayoutManager layoutManager = new LinearLayoutManager(MainActivity.this);
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerView.setLayoutManager(layoutManager);
// Set the adaptor
                CountryAdapter adapter = new CountryAdapter();
                recyclerView.setAdapter(adapter);
// Load data into the adapter.

                adapter.setCountryList(country);
                adapter.setOnCountryClickListener(new OnCountryClickListener() {

                    @Override
                    public void onNameClicked(final Country country) {
                        Toast.makeText(MainActivity.this, "La capitale de " + country.getName() + " est " + country.getCountryCapital(), Toast.LENGTH_LONG).show();
                    }
                    @Override
                    public void onFlagClicked(final Country country)
                    {
                        Toast.makeText(MainActivity.this, "C'est le drapeau de " + country.getName(), Toast.LENGTH_LONG).show();
                    }
                });



            }
            @Override
            public void onFailure(Call<List<Country>> call, Throwable t) {
                TextView test=findViewById(R.id.test);
                test.setText("t");
                Log.d("joyPAD",t.getMessage());
            }

        });

}

}


