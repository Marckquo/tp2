package DETOURBE.MarcAntoine.tp2;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MyService
{
    @GET("rest/v2/all")
    Call<List<Country>> getCountry();

}
