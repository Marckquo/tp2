package DETOURBE.MarcAntoine.tp2;

public interface OnCountryClickListener
{
    void onFlagClicked(Country country);
    void onNameClicked(Country country);
}

